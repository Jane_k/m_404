
public class aufg3b {
	
	public static void main(String[] args) {
		
		int[] a = new int[] {1, 2, 3, 10392};
		
		int[] b = revrs(a);
		
		for(int i = 0; i<a.length; i++) {
			System.out.print(a[i] + ", ");
		}
		
		System.out.println();
		
		for(int i = 0; i<b.length; i++) {
			System.out.print(b[i]+ ", ");
		}
	}
	
	public static int[]revrs(int[] ar){
		int[]res = new int[ar.length];
		
		for(int i = 0; i<ar.length; i++) {
			res[i] = ar[ar.length-1 - i];
		}
		
		return res;
	}
}
