import java.util.Scanner;

public class aufg3 { 		// matrix mult. schoen
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int n1 = 0;
		int m1 = 0;
		int n2 = -1;
		int m2 = -1;
		
		//  scanning numbers

		do {

			System.out.println("Geben Sie die Groesse der 1. nxm-Matrix an");
			System.out.println("n: ");
			n1 = sc.nextInt();
			System.out.println("m: ");
			m1 = sc.nextInt();

			System.out.println("Geben Sie nun die Groesse der 2. nxm-Matrix an");
			System.out.println("n: ");
			n2 = sc.nextInt();
			System.out.println("m: ");
			m2 = sc.nextInt();

			System.out.println("Es beginnt von vorne, wenn Sie ungueltige Zahlen verwenden");


		} while(m1 != n2 || (m1<1 || m2<1 || n1 <1 || n2<1));
		
		System.out.println("richtig------------richtig");

		int[][] mat1 = new int[n1][m1];
		int[][] mat2 = new int[n2][m2];

		
		//  fill mat1
		for(int i = 0; i<n1; i++) {
			int cnt = i + 1;
			System.out.println("Geben Sie nun die "+ cnt + ".Reihe der 1. Matrix an:");


			for(int o = 0; o<m1; o++) {
				int numm = sc.nextInt();
				mat1[i][o] = numm;

				for (int u = 0; u < mat1.length; u++) {
					for (int j = 0; j < mat1[u].length; j++) {
						System.out.print(mat1[u][j] + " ");
					}
					System.out.println();
				}
			}
		}
		
		
		//  fill mat2
		for(int i = 0; i<n2; i++) {
			int cnt = i + 1;
			System.out.println("Geben Sie nun die "+ cnt + ".Reihe der 2. Matrix an:");


			for(int o = 0; o<m2; o++) {
				int numm = sc.nextInt();
				mat2[i][o] = numm;

				for (int u = 0; u < mat2.length; u++) {
					for (int j = 0; j < mat2[u].length; j++) {
						System.out.print(mat2[u][j] + " ");
					}
					System.out.println();
				}
			}
		}
		
		int [][] res = matmu(mat1, mat2, n1, m1, n2, m2);
		
		System.out.println("+++++++++++++");
		
		for (int u = 0; u < res.length; u++) {
			for (int j = 0; j < res[u].length; j++) {
				System.out.print(res[u][j] + " ");
			}
			System.out.println();
		}
		

	}
	
	
	public static int[][] matmu(int[][] mat1, int[][] mat2, int n1, int m1, int n2, int m2) {
		int [][] res = new int[mat1.length][mat2[0].length];
		
		
		for(int row = 0; row<res.length; row++) {
			for(int col = 0; col<res[row].length; col++) {
				int sth = 0;
				
				for(int m = 0; m<mat2.length; m++) {
						sth += mat1[row][m] * mat2[m][col];
					
				}
				
				res[row][col] = sth;
			}
		}
		
		return res; 			// bleh
	}
	
}
